<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class movie_rentals extends Model
{
    protected $table = 'movie_rentals';
    protected $fillable = ['movie_id','rental_id','price','observations'];
    protected $guarded = ['id'];
}
