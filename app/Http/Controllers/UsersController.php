<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;

class UsersController extends Controller
{
    public function index()
    {
        $User = User::all();
        return view("users.list")->with("users",$User);
        //return \View::make('movies/list',compact('movies'));
    }

    public function update($id, Request $request)
    {
        $User = User::find($id);
        $User->name=$request->name;
        $User->email=$request->email;
        $User->password=$request->passwors;
        $User->status_id=$request->status_id;
        $User->role_id=$request->role_id;
        $User->save();
        return redirect('users');
    }

    public function edit($id)
    {
        $User = User::find($id);
        return view("users.update")->with("users",$User);
    }

    public function show (Request $request)
    {
        $User = User::where('name','like','%'.$request->name.'%')->get();
        return view("users.list")->with("users",$User);
    }

    public function destroy($id)
    {
        $User = User::find($id);
        $User->delete();
        return redirect()->back();
    }

    public function create()
    {
        $User = User::all();
        return view("users.new")->with("users",$User);
        //return \View::make('movies/new',compact('movies'));
    }

    public function store (Request $request)
    {
        $User = new User;
        $User->create($request->all());
        return redirect('users');
    }
}
