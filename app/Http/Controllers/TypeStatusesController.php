<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\type_statuses as TypeStatus;

class TypeStatusesController extends Controller
{
    public function index()
    {
        $typestatus = TypeStatus::all();
        return view("typestatus.list")->with("typestatus",$typestatus);
    }

    public function update($id, Request $request)
    {
        $typestatus = TypeStatus::find($id);
        $typestatus->name=$request->name;
        $typestatus->save();
        return redirect('type_statuses');
    }

    public function edit($id)
    {
        $typestatus = TypeStatus::find($id);
        return view("typestatus.update")->with("typestatus",$typestatus);
    }

    public function show(Request $request)
    {
        $typestatus = TypeStatus::where('name','like','%'.$request->name.'%')->get();
        return view("typestatus.list")->with("typestatus",$typestatus);
    }

    public function destroy($id)
    {
        $typestatus = TypeStatus::find($id);
        $typestatus->delete();
        return redirect()->back();
    }

    public function create()
    {
        $typestatus = TypeStatus::all();
        return view("typestatus.new")->with("typestatus",$typestatus);
    }

    public function store(Request $request)
    {
        $typestatus = new TypeStatus;
        $typestatus->create($request->all());
        return redirect('typestatus');
    }
}
