<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Status as Status;

class StatusController extends Controller
{
    public function index()
    {
        $status = Status::all();
        return view("status.list")->with("status",$status);
    }

    public function update($id, Request $request)
    {
        $status = Status::find($id);
        $status->name=$request->name;
        $status->type_status_id=$request->type_status_id;
        $status->save();
        return redirect('status');
    }

    public function edit($id)
    {
        $status = Status::find($id);
        return view("status.update")->with("status",$status);
    }

    public function show(Request $request)
    {
        $status = Status::where('name','like','%'.$request->name.'%')->get();
        return view("status.list")->with("status",$status);
    }

    public function destroy($id)
    {
        $status = Status::find($id);
        $status->delete();
        return redirect()->back();
    }

    public function create()
    {
        $status = Status::all();
        return view("status.new")->with("status",$status);
    }

    public function store(Request $request)
    {
        $status = new Status;
        $status->create($request->all());
        return redirect('status');
    }
}
