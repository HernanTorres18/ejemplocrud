<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie as Movie;

class MovieController extends Controller
{
    public function index()
    {
        $movies = Movie::all();
        return view("movies.list")->with("movies",$movies);
        //return \View::make('movies/list',compact('movies'));
    }

    public function update($id, Request $request)
    {
        $movie = Movie::find($id);
        $movie->name=$request->name;
        $movie->description=$request->description;
        $movie->user_id=$request->user_id;
        $movie->status_id=$request->status_id;
        $movie->save();
        return redirect('movie');
    }
    
    public function edit($id)
    {
        $movie = Movie::find($id);
        return view("movies/update")->with("movie",$movie);
    }

    public function show (Request $request)
    {
        $movies = Movie::where('name','like','%'.$request->name.'%')->get();
        return view("movies.list")->with("movies",$movies);
    }

    public function destroy($id)
    {
        $movie = Movie::find($id);
        $movie->delete();
        return redirect()->back();
    }

    public function create()
    {
        $movies = Movie::all();
        return view("movies.new")->with("movies",$movies);
        //return \View::make('movies/new',compact('movies'));
    }

    public function store (Request $request)
    {
        $movies = new Movie;
        $movies->create($request->all());
        return redirect('movie');
    }
}
