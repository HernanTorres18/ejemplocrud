<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\rental as rental;

class RentalController extends Controller
{
    public function index()
    {
        $rental = rental::all();
        return view("rentals.list")->with("rentals",$rental);
        //return \View::make('movies/list',compact('movies'));
    }

    public function update($id, Request $request)
    {
        $rental = rental::find($id);
        $rental->start_date=$request->start_date;
        $rental->end_date=$request->end_date;
        $rental->total=$request->total;
        $rental->user_id=$request->user_id;
        $rental->status_id=$request->status_id;
        $rental->save();
        return redirect('rental');
    }

    public function edit($id)
    {
        $rental = rental::find($id);
        return view("rentals.update")->with("rentals",$rental);
    }

    public function show (Request $request)
    {
        $rental = rental::where('start_date','like','%'.$request->start_date.'%')->get();
        return view("rentals.list")->with("rentals",$rental);
    }

    public function destroy($id)
    {
        $rental = rental::find($id);
        $rental->delete();
        return redirect()->back();
    }

    public function create()
    {
        $rental = rental::all();
        return view("rentals.new")->with("rentals",$rental);
        //return \View::make('movies/new',compact('movies'));
    }

    public function store (Request $request)
    {
        $rental = new rental;
        $rental->create($request->all());
        return redirect('rental');
    }
     
}
