<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\categories as categories;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = categories::all();
        return view("categories.list")->with("categories",$categories);
    }

    public function update($id, Request $request)
    {
        $categories = categories::find($id);
        $categories->name=$request->name;
        $categories->status_id=$request->status_id;
        $categories->save();
        return redirect('categories');
    }

    public function edit($id)
    {
        $categories = categories::find($id);
        return view("categories.update")->with("categories",$categories);
    }

    public function show (Request $request)
    {
        $categories = categories::where('name','like','%'.$request->name.'%')->get();
        return view("categories.list")->with("categories",$categories);
    }

    public function destroy($id)
    {
        $categories = categories::find($id);
        $categories->delete();
        return redirect()->back();
    }

    public function create()
    {
        $categories = categories::all();
        return view("categories.new")->with("categories",$categories);
        //return \View::make('movies/new',compact('movies'));
    }

    public function store (Request $request)
    {
        $categories = new categories;
        $categories->create($request->all());
        return redirect('categories');
    }
}
