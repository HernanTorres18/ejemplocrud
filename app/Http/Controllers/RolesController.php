<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\roles as roles;

class RolesController extends Controller
{
    public function index()
    {
        $roles = roles::all();
        return view("roles.list")->with("roles",$roles);
    }

    public function update($id, Request $request)
    {
        $roles = roles::find($id);
        $roles->name=$request->name;
        $roles->status_id=$request->status_id;
        $roles->save();
        return redirect('roles');
    }

    public function edit($id)
    {
        $roles = roles::find($id);
        return view("roles.update")->with("roles",$roles);
    }

    public function show (Request $request)
    {
        $roles = roles::where('name','like','%'.$request->name.'%')->get();
        return view("roles.list")->with("roles",$roles);
    }

    public function destroy($id)
    {
        $roles = roles::find($id);
        $roles->delete();
        return redirect()->back();
    }

    public function create()
    {
        $roles = roles::all();
        return view("roles.new")->with("roles",$roles);
        //return \View::make('movies/new',compact('movies'));
    }

    public function store (Request $request)
    {
        $roles = new roles;
        $roles->create($request->all());
        return redirect('roles');
    }
}
