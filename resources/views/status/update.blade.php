@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <section class="container">
        <div class="row">
            <article class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                            <a href="\status" class="btn btn-primary">Regresar</a>
                    </div>
                <form action='{{ url("status/update/$status->id") }}' method="post">
                @csrf
                    <div class="form-group">
                        <h1>Actualizar Status</h1>
                        <label>Nombre</label>
                        <input type="text" name="name" class="form-control" required value="{{$status->name}}">
                    </div>
                    <div class="form-group">
                        <label>id type status</label>
                        <input type="text" name="type_status_id" class="form-control" required value="{{$status->type_status_id}}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Enviar</button>
                    </div>
                </form>
            </article>
        </div>
    </section>    
@endsection