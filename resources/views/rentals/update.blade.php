@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <section class="container">
        <div class="row">
            <article class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                            <a href="\rental" class="btn btn-primary">Regresar</a>
                    </div>
                <form action='{{ url("rentals/update/$rental->id") }}' method="post">
                @csrf
                    <div class="form-group">
                        <h1>Actualizar Renta</h1>
                        <label>Fecha Inicial</label>
                        <input type="date" name="start_date" class="form-control" required value="{{$rental->start_date}}">
                    </div>
                    <div class="form-group">
                        <label>Fecha Final</label>
                        <input type="date" name="end_date" class="form-control" required value="{{$rental->end_date}}">
                    </div>
                    <div class="form-group">
                        <label>Total</label>
                        <input type="text" name="total" class="form-control" required value="{{$rental->total}}">
                    </div>
                    <div class="form-group">
                        <label>Usuario Id</label>
                        <input type="text" name="user_id" class="form-control" required value="{{$rental->user_id}}">
                    </div>
                    <div class="form-group">
                        <label>Status Id</label>
                        <input type="text" name="status_id" class="form-control" required value="{{$rental->status_id}}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Enviar</button>
                    </div>
                </form>
            </article>
        </div>
    </section>    
@endsection