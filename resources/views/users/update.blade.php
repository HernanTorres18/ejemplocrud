@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <section class="container">
        <div class="row">
            <article class="col-md-10 col-md-offset-1">
                    <div class="form-group">
                            <a href="\User" class="btn btn-primary">Regresar</a>
                    </div>
                <form action='{{ url("User/update/$movie->id") }}' method="post">
                @csrf
                    <div class="form-group">
                        <h1>Actualizar Usuario</h1>
                        <label>Nombre</label>
                        <input type="text" name="name" class="form-control" required value="{{$user->name}}">
                    </div>
                    
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="description" class="form-control" required value="{{$user->email}}">
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="text" name="description" class="form-control" required value="{{$user->password}}">
                    </div>

                    <div class="form-group">
                        <label>Status Id</label>
                        <input type="text" name="description" class="form-control" required value="{{$user->status_id}}">
                    </div>

                    <div class="form-group">
                        <label>Rol Id</label>
                        <input type="text" name="description" class="form-control" required value="{{$user->role_id}}">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Enviar</button>
                    </div>
                </form>
            </article>
        </div>
    </section>    
@endsection