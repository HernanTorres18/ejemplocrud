@extends('layouts.app')
@section('content')
    <section class="container">
        <div class="row">
            <article class="col-md-12">
                <form action="{{route('users/show')}}" method="POST" novalidate class="form-inline">
                @csrf
                    <div class="form-group">
                        <Label>Nombre</Label>
                        <input type="text" name="name" class="form-control" namme="name">
                    </div>
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Buscar</button>
                        <a href="{{route('users.index')}}" class="btn btn-primary">Todo</a>
                        <a href="{{route('users.create')}}" class="btn btn-primary">Crear</a>
                    </div>
                </form>
            </article>
            <article class="col-md-12">
                <table class="table table-condensed table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>password</th>
                            <th>Status id</th>
                            <th>Rol id</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $users)
                        <tr>
                            <td>{{$users->name}}</td>
                            <td>{{$users->email}}</td>
                            <td>{{$users->password}}</td>
                            <td>{{$users->status_id}}</td>
                            <td>{{$users->role_id}}</td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="{{route('users.edit', ['id' => $users->id])}}" >Editar</a>
                                <a class="btn btn-danger btn-xs" href="{{route('users/destroy', ['id' => $users->id])}}" >Eliminar</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </article>
        </div>
    </section>
@endsection