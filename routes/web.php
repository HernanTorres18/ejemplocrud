<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('movie', 'MovieController');
Route::get('movie/destroy/{id}' , ['as' => 'movie/destroy' , 'uses' => 'MovieController@destroy']);
Route::post('movie/show' , ['as' => 'movie/show' , 'uses' => 'MovieController@show']);
Route::post('movie/update/{id}' , ['as' => 'movie/update' , 'uses' => 'MovieController@update']);
Route::get('movie/edit/{id}' , ['as' => 'movie.edit' , 'uses' => 'MovieController@update']);

//Categories

Route::resource('categories', 'CategoriesController');
Route::get('categories/destroy/{id}' , ['as' => 'categories/destroy' , 'uses' => 'CategoriesController@destroy']);
Route::post('categories/show' , ['as' => 'categories/show' , 'uses' => 'CategoriesController@show']);
Route::post('categories/update/{id}' , ['as' => 'categories/update' , 'uses' => 'CategoriesController@update']);
Route::get('categories/edit/{id}' , ['as' => 'categories.edit' , 'uses' => 'CategoriesController@update']);

//Rental

Route::resource('rental', 'RentalController');
Route::get('rental/destroy/{id}' , ['as' => 'rental/destroy' , 'uses' => 'RentalController@destroy']);
Route::post('rental/show' , ['as' => 'rental/show' , 'uses' => 'RentalController@show']);
Route::post('rental/update/{id}' , ['as' => 'rental/update' , 'uses' => 'RentalController@update']);
Route::get('rental/edit/{id}' , ['as' => 'rental.edit' , 'uses' => 'RentalController@update']);

//Roles

Route::resource('roles', 'RolesController');
Route::get('roles/destroy/{id}' , ['as' => 'roles/destroy' , 'uses' => 'RolesController@destroy']);
Route::post('roles/show' , ['as' => 'roles/show' , 'uses' => 'RolesController@show']);
Route::post('roles/update/{id}' , ['as' => 'roles/update' , 'uses' => 'RolesController@update']);
Route::get('roles/edit/{id}' , ['as' => 'roles.edit' , 'uses' => 'RolesController@update']);

//Status

Route::resource('status', 'StatusController');
Route::get('status/destroy/{id}' , ['as' => 'status/destroy' , 'uses' => 'StatusController@destroy']);
Route::post('status/show' , ['as' => 'status/show' , 'uses' => 'StatusController@show']);
Route::post('status/update/{id}' , ['as' => 'status/update' , 'uses' => 'StatusController@update']);
Route::get('status/edit/{id}' , ['as' => 'status.edit' , 'uses' => 'StatusController@update']);

//TypeStatus

Route::resource('typestatus', 'TypeStatusesController');
Route::get('typestatus/destroy/{id}' , ['as' => 'typestatus/destroy' , 'uses' => 'TypeStatusesController@destroy']);
Route::post('typestatus/show' , ['as' => 'typestatus/show' , 'uses' => 'TypeStatusesController@show']);
Route::post('typestatus/update/{id}' , ['as' => 'typestatus/update' , 'uses' => 'TypeStatusesController@update']);
Route::get('typestatus/edit/{id}' , ['as' => 'typestatus.edit' , 'uses' => 'TypeStatusesController@update']);

//users

Route::resource('users', 'UsersController');
Route::get('users/destroy/{id}' , ['as' => 'users/destroy' , 'uses' => 'UsersController@destroy']);
Route::post('users/show' , ['as' => 'users/show' , 'uses' => 'UsersController@show']);
Route::post('users/update/{id}' , ['as' => 'users/update' , 'uses' => 'UsersController@update']);
Route::get('users/edit/{id}' , ['as' => 'users.edit' , 'uses' => 'UsersController@update']);