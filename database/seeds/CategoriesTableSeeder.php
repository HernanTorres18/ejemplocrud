<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\categories;
use Illuminate\Support\Facades\Hash;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0; $i < 10; $i++) {
            $movie = new categories;
            $movie->name = $faker->name;
            $movie->status_id = 1;
            $movie->save();
        }
    }
}
