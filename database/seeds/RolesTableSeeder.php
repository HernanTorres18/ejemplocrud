<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\roles;
use Illuminate\Support\Facades\Hash;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0; $i < 10; $i++) {
            $user = new roles;
            $user->name = $faker->name;
            $user->status_id = 1;
            $user->save();
        }
    }
}
