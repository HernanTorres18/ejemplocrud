<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\movie_rentals;
use Illuminate\Support\Facades\Hash;

class MovieRentalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0; $i < 10; $i++) {
            $movie = new movie_rentals;
            $movie->movie_id = 1;
            $movie->rental_id = 1;
            $movie->price = mt_rand(1,100000);
            $movie->observations = $faker->text;
            $movie->save();
        }
    }
}
