<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\rental;
use Illuminate\Support\Facades\Hash;

class RentalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0; $i < 10; $i++) {
            $user = new Rental;
            $user->start_date = $faker->date;
            $user->end_date = $faker->date;
            $user->total = mt_rand(1000,50000);
            $user->user_id = 1;
            $user->status_id = 1;
            $user->save();
        }
    }
}
