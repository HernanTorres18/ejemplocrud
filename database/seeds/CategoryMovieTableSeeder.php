<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\category_movie;
use Illuminate\Support\Facades\Hash;

class CategoryMovieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0; $i < 10; $i++) {
            $movie = new category_movie;
            $movie->movie_id = 1;
            $movie->category_id = 1;
            $movie->save();
        }
    }
}
